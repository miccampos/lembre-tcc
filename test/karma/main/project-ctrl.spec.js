'use strict';

describe('module: main, controller: ProjectCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var ProjectCtrl;
  beforeEach(inject(function ($controller) {
    ProjectCtrl = $controller('ProjectCtrl');
  }));

  it('should do something', function () {
    expect(!!ProjectCtrl).toBe(true);
  });

});
